# Install gitlab-ce with ansible.

# Setup hosts

Create a hosts file.

    [gitlab-ce]
    gitlab-ce.example.com

Or do some customization.

    [gitlab-ce]
    gitlab-ce-8-17rc1 ansible_host=X.X.X.X ansible_repo=unstable ansible_version=8.17.0-rc1.ce.0
